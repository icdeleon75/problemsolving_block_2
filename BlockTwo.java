import java.util.Scanner;

public class BlockTwo{
    public static void main(String[] args){
        fromOneToReadNumber(-5);
        allPairsBetween(-10);
        allExactDividers(-30);
        allIntegersBetweenTwoIntegers(67, 73);
        allEndingInFour(6, 90);
        fromOneToAllDigits(345);
        allIntegerFromOneToOneThousand();
        pairsFromTwntyToTwoThousand();
        endingInSix();
        sumFromOneToReadNumber(6);
        digitInInteger(29);
        haveDigitOne(541);
        multiplesOfFive(79);
        multipleOfThree();
        sumMultipleOfThree();
        firstMultipleOfThree(15);
        biggestAverage(6, 6);
        multiplesOfFiveBetween(-86, 47);
        isPrime(0);
        howManyDigits(-82384);
        sumOfDigits(333);
        howManyTimes(1212141);
        sumOfDigitsIsPrime(6472);
        sumOfPairs(2233);
        averageOfDigits(5786);
        biggestDigit(45783);
        hasMoreDigits(567,234567);
        hasMorePrimeDigits(776,7777);
        firstDigit(46478585);
        numericalComponents(570);
        loopInverse();
        tableMultiplicationForFive();
        //averageForEndInFive(5);
        //averageForPrimeNumbers(5);
        primeMoreClouser();
        mutualProduct(456,2838);
        allMultiplicationTables();
        multiplicationTable(50);
        fibonacci();
        isInFibonacci(14);
        fibonacciSum();
        fibonacciAverage();
        howManyElements();
        factorial(4);
        allIntegerFactorial(6);
        allIntegerAverages(6);
        allFactorialSum(4);
    }

    // 1 - Read an integer and show all the integers between 1 and the read number.
    static void fromOneToReadNumber(int num){
        System.out.println("Exercise 1");
        if(num < 0){
            for(int i =-1;i>num;i--){
                System.out.println(i);
            }
        }else{
            for(int i =1;i<num;i++){
                System.out.println(i);
            }
        }
    }

    // 2 - Read an integer and show all the pairs integers between 1 and the read number.
    static void allPairsBetween(int num){
        System.out.println("Exercise 2");
        if(num < 0){
            for(int i =-2;i>num;i =i-2){
                System.out.println(i);
            }
        }else{
            for(int i =2;i<num;i=i+2){
                System.out.println(i);
            }
        }
    }

    // 3 -  Read an integer and show all the exact dividers between 1 and the read number.
    static void allExactDividers(int num){
        System.out.println("Exercise 3");
        if(num < 0){
            for(int i= -1;i > num;i--){
                if(num % i ==0){
                    System.out.println(i);
                }
            }
        }else{
            for(int i =1;i <num;i++){
                if(num % i ==0){
                    System.out.println(i);
                }
            }
        }
    }

    // 4 - Read two integers and show all the integers between they.
    static void allIntegersBetweenTwoIntegers(int numOne, int numTwo){
        System.out.println("Exercise 4");
        if(numOne > numTwo){
            for(int i =numTwo+1;i<numOne;i++){
                System.out.println(i);
            }
        }else if(numTwo > numOne){
            for(int i =numOne+1;i<numTwo;i++){
                System.out.println(i);
            }
        }else{
            System.out.println("The provided numbers are equal");
        }
    }

    // 5 - Read two integers and show all the numbers ending in 4 between they.
    static void allEndingInFour(int numOne, int numTwo){
        System.out.println("Exercise 5");
        if(numOne > numTwo){
            for(int i = numTwo;i< numOne;i++){
                if(i % 10 == 4){
                    System.out.println(i);
                }
            }
        }else if(numTwo > numOne){
            for(int i = numOne;i < numTwo;i++){
                if(i % 10 == 4){
                    System.out.println(i);
                }
            }
        }else{
            System.out.println("The provided numbers can't be equal.");
        }
    }

    // 6 - Read a three-digits integer and show all the integers between 1 and the three digits.
    static void fromOneToAllDigits(int num){
        System.out.println("Excercise 6");
        if(num < 100 || num > 999){
            System.out.println("The number must be a three-digits integer");
            return;
        }

        for(int i=1;i < 4;i++){
            System.out.println("all the integer from 1 to "+(num % 10));
            for(int o =1;o < (num % 10);o++){
                System.out.println(o);
            }
            num = num / 10;
        }
    }

    // 7 - Show all the integers between 1 and 100.
    static void allIntegerFromOneToOneThousand(){
        System.out.println("Excercise 7");
        for(int i =1;i<100;i++){
            System.out.print(i+" ");
        }
        System.out.println("");;
    }

    // 8 - Show all the pairs between 20 and 200.
    static void pairsFromTwntyToTwoThousand(){
        System.out.println("Exercise 8");
        for(int i =20;i < 200;i+=2){
            System.out.print(i+" ");
        }
    }

    // 9 - Show all the numbers ending in 6 between 25 and 205.
    static void endingInSix(){
        System.out.println("Exercise 9");
        for(int i =25;i < 205; i++){
            if(i % 10 == 6)
                System.out.println(i);
        }
    }

    // 10 - Read an integer and show the sum of all the numbers from 1 to the read number.
    static void sumFromOneToReadNumber(int num){
        System.out.println("Exercise 10");
        int sum =0;
        if(num < 0){
            for(int i =-1;i > num; i--){
                sum = sum + i;
            }
        }else{
            for(int i =1;i < num; i++){
                sum = sum + i;
            }
        }
        System.out.println("The sum of all the digits from 1 to "+num+" is equal to: "+sum);
    }

    // 11 - Read a two-digits integer and show all the integers between both digits
    static void digitInInteger(int num){
        System.out.println("Exercise 11");
        if(num > 99 || num < 10){
            System.out.println("The number must be a two-digits integer");
            return;
        }
        System.out.println("Provided number: "+num);
        if(num / 10 > num % 10){
            for(int i = (num % 10); i < (num / 10);i++){
                System.out.println(i);
            }
        }else if(num % 10 > num / 10){
            for(int i = (num / 10); i < (num % 10);i++){
                System.out.println(i);
            } 
        }else{
            System.out.println("The digits of the provided number are equal "+num);
        }
    }

    // 12 -  Read a three-digits integer and determine if has the digit "1"
    static void haveDigitOne(int num){
        System.out.println("Exercise 12");

        if(num > 999 || num < 100){
            System.out.println("The provided number must be three-digits integer");
            return;
        }
        boolean one =false;

        while(num != 0){
            if(num % 10 == 1){
                one =true;
            }
            num = num / 10;
        }

        if(one)
            System.out.println("The provided number have the digit \"1\"");
        else
            System.out.println("The provided number does not have the digit \"1\"");      
    }

    // 13 - Read an integer and show all multiples of five between 1 and the provided number.
    static void multiplesOfFive(int num){
        System.out.println("Exercise 13");

        if(num < 0)
            num = num * -1;

        for(int i =1;i < num;i++){
            if(i % 5 ==0){
                System.out.print(i+" ");
            }
        }
        System.out.println();
    }

    // 14 - Show the first 20 multiples of 3
    static void multipleOfThree(){
        System.out.println("Exercise 14");
        int counter=1,current = 3;
        while(counter != 20){
            if(current % 3 ==0){
                System.out.println(current);
                counter++;
            }
            current++;
        }
    }

    // 15 - Print the sum of the first 20 multiples of 3
    static void sumMultipleOfThree(){
        System.out.println("Exercise 15.");
        int counter=1,current = 3, sum =0;
        while(counter != 20){
            if(current % 3 ==0){
                sum = sum + current;
                counter++;
            }
            current++;
        }
        System.out.println("The sum of the first 20 multiples of three is: "+sum);
    }

    // 16 - display the average of first n multiples of 3 for a number n read
    static void firstMultipleOfThree(int num){
        System.out.println("Exercise 16");
        int counter=0,sum=0;
        for(int i=3;i<num;i++){
            if(i % 3 ==0){
                sum = sum +i;
                counter++;
            }
        }
        System.out.println("The avarege of the first "+counter+" multiple of three is equal to: "+(sum / counter));
    }

    // 17 - average the first x multiples of 2 and determine if that average is greater than the first y multiples of 5.
    static void biggestAverage(int multiplesForTwo, int multiplesForFive){
        System.out.println("Exercise 17");
        int counterForTwo=0,counterForFive=0,averageForTwo=0,averageForFive=0, currentTwo=1,currentFive=1;
        
        while(counterForTwo != multiplesForTwo){
            if(currentTwo % 2 ==0){
                averageForTwo = averageForTwo + currentTwo;
                counterForTwo++;
            }
            currentTwo++;
        }

        while(counterForFive != multiplesForFive){
            if(currentFive % 5 ==0){
                averageForFive = averageForFive + currentFive;
                counterForFive++;
            }
            currentFive++;
        }

        if(averageForFive > averageForTwo){
            System.out.println("The average for the first "+multiplesForFive+" multiples of 5 is biggest "+
             "than the average for first "
            +multiplesForTwo+" multiples of two: "+averageForFive+" vs "+averageForTwo);
        } else if(averageForTwo > averageForFive){
            System.out.println("The average for the first "+multiplesForTwo+" multiples of 2 is biggest "+
             "than the average for first "
            +multiplesForFive+" multiples of two: "+averageForTwo+" vs "+averageForFive);
        }else{
            System.out.println("The averges are equal");
        }

    }

    // 18 -  Read two integers and show all the multiples of five between the smallest and the biggest.
    static void multiplesOfFiveBetween(int numOne, int numTwo){
       System.out.println("Exercise 18");
        if(numOne > numTwo){
            for(int i=numTwo; i < numOne;i++){
                if(i % 5 ==0)
                System.out.println(i);
            }
        } else if(numOne == numTwo){
            System.out.println("The digits are equal. Dont have integers between");
        }else{
            for(int i=numOne; i < numTwo;i++){
                if(i % 5 ==0)
                System.out.println(i);
            }
        }
    }

    // 19 - Read an integer and determine if is prime.
    static void isPrime(int num){
        System.out.println("Exercise 19");
        int counter=0;
        if(num < 0){
            for(int i = -1;i >= num; i--){
                if(num % i ==0)
                   counter++;
            }
        }else{
            for(int i=1;i<=num;i++){
                if(num % i == 0)
                    counter++;
            }
        }
        
        if(counter <=2)
            System.out.println("The number "+num+" Is prime");
        else{
            System.out.println("The number "+num+" is not prime");
        }
    }

    // 20 -  Read an integer and determine how many digits it has.
    static void howManyDigits(int num){
        System.out.println("Exercise 20");
        int counter=0;
        while(num !=0){
            num = num / 10;
            counter++;
        }
        System.out.println("The provided number has "+counter+" digits.");
    }

    // 21 - Read an integer and determine how much is its digits sum equal to.
    static void sumOfDigits(int num){
        System.out.println("Exercise 21");

        int sum=0;
        while(num !=0){
            sum = sum + (num % 10);
            num = num / 10;
        }        

        System.out.println("The sum of the digits for the provided number is equal to: "+sum);
    }

    // 22 - Read an integer and determine how many times it has the digit "1".
    static void howManyTimes(int num){
        System.out.println("Exercise 22");

        int oneCounter=0;

        while(num !=0){
            if(num % 10 == 1)
                oneCounter++;
            num = num / 10;
        }

        System.out.println("The digit 1 is repeat "+oneCounter+" times");
    }

    // 23 - Read an integer and determine if its digits sum is a prime number.
    static void sumOfDigitsIsPrime(int num){
        System.out.println("Exercise 23");

        int sum=0, counter=0;
        while(num !=0){
            sum = sum + (num % 10);
            num = num / 10;
        }   

        if(sum < 0)
            sum = sum * -1;

        for(int i =1;i <= sum; i++){
            if(sum % i ==0)
                counter++;
        }

        if(counter <= 2)
            System.out.println("The sum of the digits for the provided number is equal to: "+sum+" and is prime");
        else
            System.out.println("The sum of the digits for the provided number is equal to: "+sum+" and is not prime");
    }

    // 24 - Read an integer and show the sum of its digits pairs. 
    static void sumOfPairs(int num){
        System.out.println("Exercise 24");

        int sum =0;
        while(num != 0){
            if((num %10 ) % 2 ==0)
                sum = sum + num%10;
            num = num / 10;
        }

        System.out.println("The sum of the digits pair for the provided number is equal to: "+sum);
    }

    // 25 - Read an integer and determine how many is the avarage of the sum of its digits equal to.
    static void averageOfDigits(int num){
        System.out.println("Exercise 25");
        int sum =0,counter=0, copy = num;

        while(num != 0){
            sum = sum + (num % 10);
            counter++;
            num = num / 10;
        }
        System.out.println("The sum average of the digits of "+copy+" is equal to: "+(sum / counter));
    }

    // 26 - Read an integer and determine which one is the biggest digit.
    static void biggestDigit(int num){
        System.out.println("Exercise 26");

        int biggest = num % 10;
        while(num != 0){
            num = num /10;
            if(num % 10 > biggest)
                biggest = num % 10;
        }

        System.out.println("The biggest digit in the provided number is: "+biggest);

    }

    // 27 - Read two integers and determine which one has more digits.
    static void hasMoreDigits(int numOne, int numTwo){
        System.out.println("Exercise 27");

        int counterOne=0, counterTwo=0, copyOne=numOne,copyTwo=numTwo;

        while(numOne !=0){
            numOne = numOne/10;
            counterOne+=1;
        }
        while(numTwo != 0){
            numTwo = numTwo/10;
            counterTwo+=1;
        }

        if(counterOne > counterTwo)
            System.out.println("The number "+copyOne+" has more digits than "+copyTwo);
        else if(counterOne == counterTwo)
            System.out.println("The provided numbers has same number of digits");
        else{
            System.out.println("The number "+copyTwo+" has more digits than "+copyOne);
        }
    }

    // 28 - Read two integers and determine which one has more prime digits.
    static void hasMorePrimeDigits(int numOne, int numTwo){
        System.out.println("Exercise 28");
        int digitsForOne=0, primeOne=0, primeTwo=0, digitisForTwo=0;

        // set all number with a positive value
        if(numOne < 0)
            numOne = numOne * -1;     
        if(numTwo < 0)
            numTwo = numTwo * -1;

        // counting the prime digits for the first number
        while(numOne !=0){           
            for(int i=1;i <= (numOne % 10);i++){
                if((numOne % 10) % i ==0){
                    digitsForOne++;
                }
            }

            if(digitsForOne <=2)
                primeOne++;

            digitsForOne =0;
            numOne = numOne/10;
        }

        // counting the prime digits for the second number
        while(numTwo != 0){
            for(int i=1;i <= (numTwo % 10);i++){
                if((numTwo % 10) % i ==0){
                    digitisForTwo++;
                }
            }

            if(digitisForTwo <= 2)
                primeTwo++;
            
            digitisForTwo =0;
            numTwo = numTwo/10;
        }

        if(primeOne > primeTwo)
            System.out.println("The first number has more prime digits: "+primeOne);
        else if(primeTwo > primeOne)
            System.out.println("The second number has more prime digits: "+primeTwo);
        else
            System.out.println("The provided number has same number of prime digits");
    }

    // 29 - Read an integer and print its first digit.
    static void firstDigit(int num){
        System.out.println("Exercise 29");
        if(num <0)
            num = num * -1;

        while(num > 9){
            num = num /10;
        }
        System.out.println("The first digit is "+num);
    }

    // 30 - Read an integer and show all its numerical components, that is, those numbers for which it is a multiple.
    static void numericalComponents(int num){
        System.out.println("Exercise 30");
        if(num < 0){
            for(int i =-1;i > num;i--){
                if(num % i ==0){
                    System.out.println(num +" is multiple of "+i);
                }
            }
        }else{
            for(int i =1;i < num; i++){
                if(num % i ==0){
                    System.out.println(num +" is multiple of "+i);
                }
            }
        }
    }

    // 31 - Read numbers until you enter 0 and determine what the average of the numbers ending in 5 is equal to.
    static void averageForEndInFive(int num){
        System.out.println("Exercise 31");
        int sum=0,counter=0;
        Scanner scan = new Scanner(System.in);
        do{
            System.out.println("Enter an integer");
            num = Integer.parseInt(scan.nextLine());
            if(num % 10 == 5){
                counter++;
                sum = sum +num;
            }
        }while(num !=0);
        System.out.println("There are "+counter+" numbers that end with five. Its average is equal to: "+(sum / counter));
    }

    // 32 - read numbers until they type 0 and determine what the average of the read prime numbers is equal to.
    static void averageForPrimeNumbers(int num){
        System.out.println("Exercise 32");
        int counterPrimes=0, determinateCounter=0, sum=0;
        Scanner scan = new Scanner(System.in);
        do{
            System.out.println("Enter an integer");
            num = Integer.parseInt(scan.nextLine());
            
            determinateCounter=0;
            for(int i =1;i <= num;i++){
                if(num % i ==0){
                    determinateCounter++;
                }
            }
            if(determinateCounter <=2){
                counterPrimes++;
                sum = sum + num;
            }
        }while(num !=0);
        System.out.println(counterPrimes+" prime numbers were provided. Its average is equal to: "+(sum / counterPrimes));
    }

    // 33 - If 32768 is the ceiling for short integers, determine which is the closest prime number.
    static void primeMoreClouser(){
        System.out.println("Exercise 33");

        int counter, biggest=32768;

        while(biggest !=0){
            biggest = biggest - 1;
            counter =0;
            for(int i =1; i <= biggest; i++){
                if(biggest % i ==0){
                    counter++;
                }
            }
            if(counter <= 2){
                System.out.println("The more close number is: "+biggest);
                return;
            }             
        }
    }

    // 34 - Geneate the numbers from 1 to 10 using a loop from 10 to 1.
    static void loopInverse(){
        System.out.println("Exercise 34");       
        for(int i =10; i >=1; i--){
            int num=11;
            System.out.println(num - i);
        }
    }

    // 35 - Read two integers and determine how many is the mutual product of their first digits.
    static void mutualProduct(int numOne, int numTwo){
        System.out.println("Exercise 35");

        if(numOne < 0)
            numOne = numOne * -1;
        if(numTwo < 0)
            numTwo = numTwo * -1;

        while(numOne > 10){
            numOne = numOne / 10;
        }
        while(numTwo > 10){
            numTwo = numTwo / 10;
        }

        System.out.println("The mutual product of "+numOne+" and "+numTwo+" is equal to: "+((numOne + numTwo) / 2));
    }

    // 36 - show on the screen the multiplication table of the number 5
    static void tableMultiplicationForFive(){
        System.out.println("Exercise 36");
        for(int i=1;i <=15;i++){
            System.out.println(5+" x "+i+" = "+(5*i));
        }
    }

    // 37 - generate all the multiplication's tables from 1 to 10.
    static void allMultiplicationTables(){

        System.out.println("Excersise 37");

        for(int i =1;i < 11;i++){
            System.out.println("multiplication table of "+i);
            for(int o=1;o<11;o++){
                System.out.println(i+" x "+o+" = "+(i*o));
            }
            System.out.println("------------------------");
        }
    }

    // 38 - Read an integener and generate its multiplication table.
    static void multiplicationTable(int num){
        System.out.println("Exercise 38");

        if(num< 0)
            num = num *-1;
        
            for(int i=1;i<11;i++){
                System.out.println(num+" x "+i+" = "+(num*i));
            }
    }

    // 39 - The Fibonacci's series is defined as the series that begins with the digits 1 and 0 and progressively adds the last two digits.
    //Using cycles, print the fibonacci series until reaching or exceeding 10000
    static void fibonacci(){
        System.out.println("Excersice 39");
        int a=0, b=1, c=0;
        System.out.print(a+" "+b+" ");
        while(c <= 10000){
            c = a + b;
            a=b;
            b=c;
            System.out.print(c+" ");
        }
    }

    // 40 - Read a two-digits integer and determine if it belongs to the fibonacci's serie
    static void isInFibonacci(int num){
        System.out.println("Exercise 40");
        if(num < 10 || num > 99){
            System.out.println("The number must be a two-digits integer");
            return;
        }
        int a=0,b=1,c=0;
        boolean isInFibonacci=false;

        while(c <=100){
            c = a +b;
            a=b;
            b=c;
            if(num == c)
                isInFibonacci=true;
        }
        if(isInFibonacci)
            System.out.println("The provide number is in the fibonacci's serie");
        else
            System.out.println("The provide number isn't in the fibonacci's serie");
        
    }

    // 41 - determines how much the sum of the elements of the fibonacci series between 0 and 100 is equal to.
    static void fibonacciSum(){
        System.out.println("Exercise 41");
        int a=0,b=1,c=0,sum=0;

        while(c <= 100){
            c = a+b;
            a = b;
            b = c;
            sum = sum + c;
        }
        System.out.println("The sum of the elements of the fibonacci series between 0 and 100 is equal to "+sum);
    }

    // 42 - determine how may the integer average of the fibonaccy series digits between 0 and 10000 is equal to.
    static void fibonacciAverage(){
        System.out.println("Exercise 42");

        int a=0,b=1,c=0,sum=0,counter=0;

        while(c <= 10000){
            c = a+b;
            a = b;
            b = c;
            sum = sum +c;
            counter++;
        }

        System.out.println("The integer average for the first: "+counter+" digits of the fibonacci's serie is "+(sum/counter));
    }

    // 43 - Determine how many elements of fibonacci's serie are between 1000 and 2000.
    static void howManyElements(){
        System.out.println("Exercise 43");
        int a=0,b=1,c=0,counter=0;

        while(c <= 2000){
            c = a+b;
            a = b;
            b = c;
            if(c > 1000 && c < 2000)
                counter++;
        }
        System.out.println("There are: "+counter+" fibonacci digits");
    }

    // 44 - Read a number and determine its factorial.
    static void factorial(int num){
        System.out.println("Exercise 44");
        int factorial = 1;
        if(num < 0){
            for(int i= -1;i >= num; i--){
                factorial = factorial * i;
            }
        }else{
            for(int i=1;i <= num;i++){
                factorial = factorial * i;
            }
        }
        System.out.println(num+"! is equal to: "+factorial);
    }

    // 45 - Read a number and show the factorial of all the integers between 1 and the readed number.
    static void allIntegerFactorial(int num){
        System.out.println("Exercise 45");
        int factorial =1;
        if(num < 0)
            num = num * -1;

        for(int i =1;i < num; i++){
            factorial =1;
            for(int o =1;o <= i;o++){
                factorial = factorial * o;
            }
            System.out.println(i+"! is equal to: "+factorial);
        }
    }

    // 46 - Read a integer and determine the integer-average of each factorial for the numbers between 1 and the provided number.
    static void allIntegerAverages(int num){
        System.out.println("Exercise 46");
        int factorial=1, sum=0;

        if(num < 0)
            num = num * -1;

        for(int i =1;i < num; i++){
            factorial = 1;
            for(int o =1; o<=i;o++){
                factorial = factorial * o;
            }
            sum = sum + factorial;
        }
        System.out.println("The average for the integers between 1 and "+num+" is equal to: "+(sum / (num -1)));
    }

    // 47 -  Read an integer and and determinate how many is the sum of all the integers factorial between 1 and the read number is equal to.
    static void allFactorialSum(int num){
        System.out.println("Exercise 47");

        if(num < 0)
            num = num * -1;

        int factorial =1,sum=0;

        for(int i=1;i<=num;i++){
            factorial =1;
            for(int o=1;o <=i; o++){
                factorial = factorial * o;
            }
            sum = sum + factorial;
        }

        System.out.println("The sum of all integer factorial between 1 and "+num+" is equal to: "+sum);
    }


}


